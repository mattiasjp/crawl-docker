FROM ubuntu:latest
MAINTAINER Mattias

ENV LANG C.UTF-8

RUN apt-get update --fix-missing && \
    apt-get -y --allow-unauthenticated upgrade && \
    apt-get -y -f --allow-unauthenticated install autoconf \
                                   build-essential \
                                   binutils-gold \
                                   bison \
                                   bzip2 \
                                   ccache \
                                   debootstrap \
                                   flex \
                                   git \
                                   libbot-basicbot-perl \
                                   libfreetype6-dev \
                                   liblua5.1-0-dev \
                                   libncurses5-dev \
                                   libncursesw5-dev \
                                   libpcre3-dev \
                                   libpng-dev \
                                   libsqlite3-dev \
                                   libsdl2-image-dev \
                                   libsdl2-mixer-dev \
                                   libsdl2-dev \
                                   libz-dev \
                                   lsof \
                                   ncurses-dev \
                                   ncurses-term \
                                   openssh-server \
                                   pkg-config \
                                   procps \
                                   python-minimal \
                                   python-pip \
                                   sudo \
                                   sqlite3 \
                                   ttf-dejavu-core \
                                   vim && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# To use jinja2 templates for configurations
RUN pip install j2cli pyyaml

COPY ./templates /templates
COPY ./template_vars.yml /template_vars.yml
COPY ./files /files

RUN useradd -m crawl && \
    useradd -m crawl-dev && \
    useradd -m terminal

###############
USER crawl-dev

# Don't touch these
ENV FOLDERS /crawl-master/webserver \
            /crawl-master/webserver/run \
            /crawl-master/webserver/sockets \
            /crawl-master/webserver/templates \
            /dgldir/data \
            /dgldir/dumps \
            /dgldir/morgue \
            /dgldir/rcfiles \
            /dgldir/ttyrec \
            /dgldir/data/menus \
            /dgldir/inprogress
ENV IP 0.0.0.0

RUN mkdir /home/crawl-dev/logs \
          /home/crawl-dev/run

WORKDIR /home/crawl-dev
RUN git clone -b http-timeouts-2.4 git://github.com/flodiebold/tornado.git && \
    git clone -b szorg git://github.com/neilmoore/dgamelaunch.git && \
    git clone -b szorg git://github.com/neilmoore/dgamelaunch-config.git && \
    git clone git://github.com/neilmoore/sizzell.git

WORKDIR /home/crawl-dev/tornado
RUN python setup.py build

# Temporary - won't have to wait for everything to get pulled while testing.
WORKDIR /home/crawl-dev/dgamelaunch-config/crawl-build
RUN git clone git://github.com/crawl/crawl.git crawl-git-repository

WORKDIR /home/crawl-dev/dgamelaunch-config/crawl-build/crawl-git-repository
RUN git submodule init && \
    git submodule update

###############
USER root

RUN chmod 666 /dev/ptmx
RUN echo "crawl-dev ALL=(root) NOPASSWD: /home/crawl-dev/dgamelaunch-config/bin/dgl, /sbin/install-trunk.sh, /sbin/install-stable.sh, /etc/init.d/webtiles, /sbin/remove-trunks.sh" >> /etc/sudoers

###############
USER crawl-dev

WORKDIR /home/crawl-dev/dgamelaunch/
RUN git checkout szorg && \
    ./autogen.sh --enable-debugfile --enable-sqlite --enable-shmem && \
    sed -i -e "s|-lsqlite3 -lrt|-lsqlite3 -lrt -pthread|gI" Makefile && \
    make VIRUS=1

###############
USER root

WORKDIR /home/crawl-dev/dgamelaunch/
RUN make install && \
    cp ee virus /bin

###############
USER root

COPY ./utils /utils
RUN cp /home/crawl-dev/dgamelaunch/dgamelaunch /bin/
RUN chown root:root /bin/dgamelaunch
RUN chmod 755 /bin/dgamelaunch
RUN echo "/bin/dgamelaunch" >> /etc/shells
RUN chsh -s /bin/dgamelaunch terminal
RUN usermod --password `openssl passwd terminal` terminal
RUN cat /utils/sshconf.txt >> /etc/ssh/sshd_config

VOLUME ["/usr/games/", "/dgldir", "/crawl-master"]
EXPOSE 8080 22

COPY docker-entrypoint.sh /usr/local/bin/
RUN ln -s /usr/local/bin/docker-entrypoint.sh /entrypoint.sh # backwards compat

ENTRYPOINT ["docker-entrypoint.sh"]
CMD service ssh start && \
    service webtiles start #&& \
#    /home/crawl-dev/sizzell/sizzell.pl
