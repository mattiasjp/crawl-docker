#!/bin/bash
set -e

if [[ "$1" == webtiles ]] || [ "$1" == sizzell.pl ]; then
  : "${VERSIONS:=0.19 0.18 0.17 0.16 0.15 0.14 0.13 0.12 0.11 0.10}"
	: "${EXPERIMENTALS:=thorn_god basajaun bosch}"

  echo $VERSIONS | sed -r 's/ /, /g'
  echo $VERSIONS | sed -r 's/ /|/g'

  # TODO use env vars here to set template_vars.yml variables

  su crawl-dev <<'EOF'
cd /home/crawl-dev/
echo $SERVERALIAS
find . -type f -print0 | xargs -0 sed -i -e "s|cszo|$SERVERLIAS|gI" \
                                         -e "s|crawl.s-z.org|$SERVERURLS|gI" \
                                         -e "s|North America|$LOCATION|" \
                                         -e "s/Neil Moore (|amethyst)/$MAINTAINER/"

cd /home/crawl-dev/dgamelaunch-config
sed -i -e "s|^export DGL_CHROOT=\/home\/crawl\/DGL|export DGL_CHROOT=\/|" \
       -e "s|^export DGL_UID.*$|export DGL_UID=$(id -u crawl)|" \
       -e "s|^export DGL_SERVER.*$|export DGL_SERVER=$SERVERURLS|" \
       -e "s|^export WEB_SAVEDUMP_URL=.*$|export WEB_SAVEDUMP_URL=http:\/\/$SERVERURLS|" \
       dgl-manage.conf
sed -i -e "s|^CRAWL_GIT_URL=.*$|CRAWL_GIT_URL=https:\/\/github.com\/crawl\/crawl.git|" crawl-git.conf
/usr/local/bin/j2 /templates/dgamelaunch.conf.j2 /template_vars.yml > dgamelaunch.conf
/usr/local/bin/j2 /templates/config.py.j2 /template_vars.yml > config.py

cd /home/crawl-dev/sizzell
/usr/local/bin/j2 /templates/sizzell.pl.j2 /template_vars.yml > sizzel.pl

cd /home/crawl-dev/dgamelaunch-config/crawl-build
# TODO: Run this only if we have experimentals, otherwise run the nonexperimental version (which is do nothing)
sed -i -e "s|^VERS_RE=.*$|VERS_RE='^[0-9]+.[0-9]+\|${EXPERIMENTALS}$'|" update-crawl-stable-build.sh

cd /home/crawl-dev/dgamelaunch-config/utils
# TODO: Run this only if we have experimentals, otherwise run the nonexperimental version (which is a TODO)
export VERSIONSCOMMA=${echo $VERSIONS | sed -r 's/ /, /g'}
export VERSIONSPIPE=${echo $VERSIONS | sed -r 's/ /|/g'}
export EXPERIMENTALSCOMMA=${echo $VERSIONS | sed -r 's/ /, /g'}
export EXPERIMENTALSPIPE=${echo $VERSIONS | sed -r 's/ /|/g'}
sed -i -e "s|^(.*)do_prompt 'trunk'.*|\1do_prompt 'trunk', '${VERSIONSCOMMA}', '${EXPERIMENTALSCOMMA}'|" \
       -e "s|^(.*)} elsif \(\$ver =~.*$|\1} elsif ($ver =~ /^${VERSIONSPIPE}|${EXPERIMENTALSPIPE}$/) {|" \
       trigger-rebuild.pl

cd /home/crawl-dev/dgamelanuch-config/chroot/bin
/usr/local/bin/j2 /templates/init-webtiles.sh.j2 /template_vars.yml > init-webtiles.sh

cd /home/crawl-dev/dgamelanuch-config/chroot/data
/usr/local/bin/j2 /templates/dgl-banner.j2 /template_vars.yml > dgl-banner

cd /home/crawl-dev/dgamelanuch-config/chroot/data/menus
/usr/local/bin/j2 /templates/experimental_menu.txt.j2 /template_vars.yml > experimental.txt
/usr/local/bin/j2 /templates/experimental_adv_menu.txt.j2 /template_vars.yml > experimental_adv.txt
/usr/local/bin/j2 /templates/main_admin.txt.j2 /template_vars.yml > main_admin.txt
/usr/local/bin/j2 /templates/main_anon.txt.j2 /template_vars.yml > main_anon.txt
/usr/local/bin/j2 /templates/main_user.txt.j2 /template_vars.yml > main_user.txt
#TODO: Run script which loops menu_adv.txt and menu.txt to replace %%version%% in file with the correct version, as well as name each file with version in filename
EOF

su crawl <<'EOF'

cd /
mkdir -p crawl-master/webserver/{run,sockets,templates}
mkdir -p dgldir/{dumps,morgue,rcfiles,ttyrec,data,inprogress}
mkdir -p dgldir/data/menus
touch dgamelaunch

# TODO: Create inprogress dir for all versions and experimentals
# TODO: Create rcfile dir for all versions and experimentals
# TODO: Create data crawl settings for all versions and experimentals
# Make sure crawl user has write access to /var/mail (0777)
EOF


# TODO In the end, add an entrypoint which starts all the servers
# TODO All files that changes (data) should be mounted on a volume container or regular volume
# TODO crawl and other folders should be volumes so they can be updated... or should they? Think this through...
# TODO Check what the stuff in utils does and what we can move in here
fi
echo $1
exec "$@"
